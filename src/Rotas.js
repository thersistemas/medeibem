import React from 'react';
import { Router, Scene } from 'react-native-router-flux';
import FontAwesome, { Icons } from 'react-native-fontawesome';

import PoliticaPrivacidade from './components/PoliticaPrivacidade';
import Sobre from './components/Sobre';
import Funcoes from './components/Funcoes';
import ViewListaPromocoes from './components/ViewListaPromocoes';
import ViewDetalhePromocao from './components/ViewDetalhePromocao';
import ViewListaClassificados from './components/ViewListaClassificados';
import ItemPodcast from './components/ItemPodcast';
import FormCadastroClassificados from './components/FormCadastroClassificados';
import FormCadastro from './components/FormCadastroUsuario';
import FormBaixaDesconto from './components/FormBaixaDesconto'
import SideMenu from './components/SideMenu';
import RadarOfertas from './components/RadarOfertas';
import FormLogin from './components/FormLogin';

export default props => (
  <Router navigationBarStyle={{ backgroundColor: '#5258aa' }} titleStyle={{ color: '#fff', alignSelf: 'center' }}>
    <Scene key='root'>
      <Scene key='formLogin' component={FormLogin} title="Login" hideNavBar={true} initial />
      <Scene key='listaPromocoes' component={ViewListaPromocoes} title="Promoções" back={true} backButtonTintColor='#fff' />
      <Scene key='detalhePromocao' component={ViewDetalhePromocao} title="Detalhe da Promoção" back={true} backButtonTintColor='#fff' />
      
      <Scene key='drawer'
        drawer={true}
        contentComponent={SideMenu}
        sceneStyle={{ backgroundColor: '#5258aa' }}
        drawerIcon={<FontAwesome style={{ fontSize: 35, color: '#fff' }}>{Icons.bars}</FontAwesome>}
      >
        <Scene key='funcoes' component={Funcoes} title="Me Dei Bem!" initial />
        <Scene key='formCadastro' component={FormCadastro} title="Cadastro" back={true} backButtonTintColor='#fff' />
        <Scene key='formCadastroClassificados' component={FormCadastroClassificados} title="Cadastro Classificado" back={true} backButtonTintColor='#fff' />
        <Scene key='formBaixaDesconto' component={FormBaixaDesconto} title="Baixa de Desconto" back={true} backButtonTintColor='#fff' />
        <Scene key='ListaClassificados' component={ViewListaClassificados} title="Classificados" back={true} backButtonTintColor='#fff' />
        <Scene key='radarOfertas' component={RadarOfertas} title="Radar de Ofertas" back={true} backButtonTintColor='#fff' />
        <Scene key='ListaPodcast' component={ItemPodcast} title="Podcasts" back={true} backButtonTintColor='#fff' />
        <Scene key='sobre' component={Sobre} title="Sobre" back={true} backButtonTintColor='#fff' />
        <Scene key='politicaPrivacidade' component={PoliticaPrivacidade} title="Politíca de Privacidade" titleStyle={{ alignSelf: 'center' }} back={true} backButtonTintColor='#fff' />
      </Scene>
    </Scene>

  </Router>
);