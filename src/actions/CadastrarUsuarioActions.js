/*
* Container com as actions que modificam os estados 
* do processo de cadastrar usuario do app.
*/

import axios from 'axios';

import { BASE_PROTOCOL, BASE_URL, BASE_API } from '../services/UrlApi';
import { 
  MODIFICA_NOME,
  MODIFICA_SOBRENOME,
  MODIFICA_EMAIL,
  MODIFICA_RADAR_ESTADO,
  ERRO_LISTAR_ESTADO,
  MODIFICA_RADAR_CIDADE,
  MODIFICA_SENHA,
  SUCESSO_CADASTRO_USUARIO,
  ERRO_CADASTRO_USUARIO
} from './types';

export const cadastraUsuario = ({ nome, sobrenome, email, radarEstado, radarCidade, senha }) => {
  console.log(nome, sobrenome, email, radarEstado, radarCidade, senha);  
  return dispatch => {  
    const formData = new FormData();
    formData.append('a', 'ru');
    formData.append('d', `{"nome":"${nome}","sobrenome":"${sobrenome}","email":"${email}","radar_uf":"${radarEstado}","radar_cid":"${radarCidade}","senha":"${senha}"}`);
    axios({
      url: `${BASE_PROTOCOL}://${BASE_URL}/${BASE_API}?`,
      method: 'POST',
      data: formData,
      headers: { Accept: 'application/json', 'Content-Type': 'multipart/form-data' }
    })
      .then(response => sucessoCadastroUsuario(response.data, dispatch))
      .catch((erro) => erroCadastroUsuario(response.data, dispatch));
  }  
}

// Action que modifica o estado do campo nome
export const modificaNome = (nome) => {
  return { type: MODIFICA_NOME, payload: nome }
}

// Action que modifica o estado do campo sobrenome
export const modificaSobrenome = (sobrenome) => {
  return { type: MODIFICA_SOBRENOME, payload: sobrenome }
}

// Action que modifica o estado do campo email
export const modificaEmail = (email) => {
  return { type: MODIFICA_EMAIL, payload: email }
}

// Action que modifica o estado do campo radar estado
export const modificaRadarEstado = (radarEstado) => {
  return { type: MODIFICA_RADAR_ESTADO, payload: radarEstado }
}

export const erroCarregarEstados = (erro, dispatch) => {
  dispatch ({ type: ERRO_LISTAR_ESTADO, payload: "Erro ao listar Estados." });
}

// Action que modifica o estado do campo radar cidade
export const modificaRadarCidade = (radarCidade) => {
  return { type: MODIFICA_RADAR_CIDADE, payload: radarCidade }
}

// Action que modifica o estado do campo senha
export const modificaSenha = (senha) => {
  return { type: MODIFICA_SENHA, payload: senha }
}

// Action sucesso cadastro usuario
export const sucessoCadastroUsuario = (response, dispatch) => {
  dispatch ({ type: SUCESSO_CADASTRO_USUARIO, payload: alert(response.sinc_msg) });
}

// Action erro cadastro usuario
export const erroCadastroUsuario = (response, dispatch) => {
  dispatch ({ type: ERRO_CADASTRO_USUARIO, payload: alert(response.sinc_msg) });
}
