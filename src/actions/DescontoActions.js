export const enviaCodigoQR = (codigoQR) => {
  return {
    type: 'envia_codigoqr',
    payload: codigoQR
  }
}