/*
* Container com as actions que modificam os estados 
* do cadastro classificados do app.
*/

import axios from 'axios';
import { openDatabase } from "react-native-sqlite-storage";

import { BASE_PROTOCOL, BASE_URL, BASE_API, APP_KEY } from '../services/UrlApi';
import {
  MODIFICA_SITUACAO,
  MODIFICA_CATEGORIA,
  MODIFICA_SUBCATEGORIA,
  MODIFICA_TITULO,
  MODIFICA_DESCRICAO,
  MODIFICA_HORARIO_INICIO_1,
  MODIFICA_HORARIO_FIM_1,
  MODIFICA_HORARIO_INICIO_2,
  MODIFICA_HORARIO_FIM_2,
  MODIFICA_TELEFONE,
  MODIFICA_EMAIL,
  LISTA_SUBCATEGORIA_VAZIA
} from './types';

/*export const verificaClassificadosBDLocal = () => {
  //return dispatch => {  
    console.log("Cheguei na função verificaClassificadosBD. :D")
    db.transaction((tx) => {
      tx.executeSql('SELECT idCLASSIF, titulo, texto, contato_horario1, contato_horario2, contato_tel, contato_email, data_cad, data_exp, situacao, obs, last_update, ind_sinc FROM CLASSIF', [], (tx, results) => {

        switch (results.rows.length) {
          case 1:
            //console.log("Cheguei switch 0. :D")
            //modificaSituacao(dispath);
            //modificaObservacao(dispatch);
          case 0:
            //console.log("Cheguei no case 0")
            verificaSituacaoClassificado()
          break;                    
        }

      });
    });
 // }  

}*/

const errorCB = (err) => {
  alert("SQL Error: " + err);
}
const successCB = () => {
  alert("SQL executed fine");
}
const openCB = () => {
  alert("Database OPENED");
}

export const cadastraClassificado = ({
    appKey,
    categoria,
    subcategoria,
    titulo,
    descricao,
    horarioInicio_1,
    horarioFim_1,
    horarioInicio_2,
    horarioFim_2,
    telefone,
    email
  }) => {
  return dispatch => {    
    const formData = new FormData();
    formData.append('a', 'rc');
    formData.append('k',`${appKey}`);
    formData.append('d', `{"categ":"${categoria}","subcateg":"${subcategoria}","titulo":"${titulo}","texto":"${descricao}","contato_h1":"${horarioInicio_1}${horarioFim_1}","contato_h2":"${horarioInicio_2}${horarioFim_2}","contato_tel":"${telefone}","contato_email":"${email}"}` )
    axios({
      url: `${BASE_PROTOCOL}://${BASE_URL}/${BASE_API}?`,
      method: 'POST',
      data: formData,
      headers: { Accept: 'application/json', 'Content-Type': 'multipart/form-data' }
    })    
      .then(response => alert(response.data.sinc_msg))
      .catch(response => alert(response.data.sinc_msg));

    console.log("Cheguei no insert")
    let db = openDatabase({name: 'medeibem.db', createFromLocation: "~me_dei_bem.db"});
    //let sqlQuery = `INSERT INTO CLASSIF (idCLASSIF, titulo, texto, contato_hora1, contato_hora2, contato_tel, contato_email, data_cad, data_exp, situacao, obs, last_update, ind_sinc) VALUES ("${titulo}", "${descricao}", "${horarioInicio_1}${horarioFim_1}", "${horarioInicio_2}${horarioFim_2}", "${telefone}", "${email}")`;
    //console.log("Insert -> "+sqlQuery);
    db.transaction((tx) => {

    //let sqlQuery = `INSERT INTO CLASSIF (idCLASSIF, titulo, texto, contato_hora1, contato_hora2, contato_tel, contato_email, data_cad, data_exp, situacao, obs, last_update, ind_sinc) VALUES ("${titulo}", "${descricao}", "${horarioInicio_1}${horarioFim_1}", "${horarioInicio_2}${horarioFim_2}", "${telefone}", "${email}", "${datetime('now')}")`;
    //console.log("Insert -> "+sqlQuery);
    tx.executeSql(`INSERT INTO CLASSIF (idCLASSIF, titulo, texto, contato_hora1, contato_hora2, contato_tel, contato_email, data_cad, data_exp, situacao, obs, last_update, ind_sinc) VALUES ("${titulo}", "${descricao}", "${horarioInicio_1}${horarioFim_1}", "${horarioInicio_2}${horarioFim_2}", "${telefone}", "${email}", "${datetime('now')}")`, successCB, errorCB);

    });

  }  
  
}

/*export const verificaSituacaoClassificado = () => {

  let db = openDatabase({name: 'medeibem.db', createFromLocation: "~me_dei_bem.db"});

  db.transaction((tx) => {
    tx.executeSql('SELECT ind_sinc FROM CLASSIF', [], (tx, results) => {
      console.log(results);
      let dados = results.rows.length;

      for (let i = 0; i < dados; i++) {

        let row = results.rows.item(i);

        if (row.ind_sinc === 1) {
          axios.get('http://admin.medeibem.mobi/app.php?a=cs&k=${app_key}')
        } 

      }
    });
  });


}*/

// Action que modifica o estado do campo situação
export const modificaSituacao = (response) => {  
  return { type: MODIFICA_SITUACAO, payload: response.situacao };
}

// Action que modifica o estado do campo observacao
const modificaObservacao = (response, dispatch) => {
  dispatch({ type: MODIFICA_OBSERVACAO, payload: response });
}

// Action que modifica o estado do campo categoria
export const modificaCategoria = (categoria) => {
  return { type: MODIFICA_CATEGORIA, payload: categoria }
}

// Action que modifica o estado do campo subcategoria
export const modificaSubcategoria = (subcategoria) => {
  return { type: MODIFICA_SUBCATEGORIA, payload: subcategoria }
}

export const listaSubcategoriaVazia = (subcategoria) => {
  return { type: LISTA_SUBCATEGORIA_VAZIA, payload: alert("Selecione uma Categoria.") }
}

// Action que modifica o estado do campo titulo
export const modificaTitulo = (titulo) => {
  return { type: MODIFICA_TITULO, payload: titulo }
}

// Action que modifica o estado do campo descricao
export const modificaDescricao = (descricao) => {
  return { type: MODIFICA_DESCRICAO, payload: descricao }
}

// Action que modifica o estado do campo horarioInicio_1
export const modificaHorarioInicio_1 = (horarioInicio_1) => {
  return { type: MODIFICA_HORARIO_INICIO_1, payload: horarioInicio_1 }
}

// Action que modifica o estado do campo horarioFim_1
export const modificaHorarioFim_1 = (horarioFim_1) => {
  return { type: MODIFICA_HORARIO_FIM_1, payload: horarioFim_1 }
}

// Action que modifica o estado do campo horarioInicio_2
export const modificaHorarioInicio_2 = (horarioInicio_2) => {
  return { type: MODIFICA_HORARIO_INICIO_2, payload: horarioInicio_2 }
}

// Action que modifica o estado do campo horarioFim_2
export const modificaHorarioFim_2 = (horarioFim_2) => {
  return { type: MODIFICA_HORARIO_FIM_2, payload: horarioFim_2 }
}

// Action que modifica o estado do campo email
export const modificaTelefone = (telefone) => {
  return { type: MODIFICA_TELEFONE, payload: telefone }
}

// Action que modifica o estado do campo email
export const modificaEmail = (email) => {
  return { type: MODIFICA_EMAIL, payload: email }
}
