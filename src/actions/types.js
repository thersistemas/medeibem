/*
* Conatiner de constantes do types das Actions.
*/

// Types Atenticação Usuario
export const MODIFICA_EMAIL = 'modifica_email';
export const MODIFICA_SENHA = 'modifica_senha';
export const LOGIN_ERRO = 'login_erro';

// Types Cadastrar Usuario
export const MODIFICA_NOME = 'modifica_nome';
export const MODIFICA_SOBRENOME = 'modifica_sobrenome';
export const MODIFICA_RADAR_ESTADO = 'modifica_radar_estado';
export const ERRO_LISTAR_ESTADO = 'erro_listar_estado';
export const MODIFICA_RADAR_CIDADE = 'modifica_radar_cidade';
export const SUCESSO_CADASTRO_USUARIO = 'sucesso_cadastro_usuario';
export const ERRO_CADASTRO_USUARIO = 'erro_cadastro_usuario';

// Types Cadastrar Classificados
export const MODIFICA_SITUACAO = 'modifica_situacao';
export const MODIFICA_OBSERVACAO = 'modifica_observacao';
export const MODIFICA_CATEGORIA = 'modifica_categoria';
export const MODIFICA_SUBCATEGORIA = 'modifica_subcategoria';
export const MODIFICA_TITULO = 'modifica_titulo';
export const MODIFICA_DESCRICAO = 'modifica_descricao';
export const MODIFICA_HORARIO_INICIO_1 = 'modifica_horario_inicio_1';
export const MODIFICA_HORARIO_FIM_1 = 'modifica_horario_fim_1';
export const MODIFICA_HORARIO_INICIO_2 = 'modifica_horario_inicio_2';
export const MODIFICA_HORARIO_FIM_2 = 'modifica_horario_fim_2';
export const MODIFICA_TELEFONE = 'modifica_telefone';
export const LISTA_SUBCATEGORIA_VAZIA = 'lista_subcategoria_vazia';
