
/*
* Container com as actions que modificam os estados 
* do processo de autenticação do app.
*/

import { openDatabase } from "react-native-sqlite-storage";
import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';

import { BASE_PROTOCOL, BASE_URL, BASE_API } from '../services/UrlApi';
import { MODIFICA_EMAIL,  MODIFICA_SENHA, LOGIN_ERRO } from './types';

/*
* Mensagens para debug
*/
const errorCB = (err) => {
  alert("SQL Error: " + err);
}
const successCB = () => {
  alert("SQL executed fine");
}
const openCB = () => {
  alert("Database OPENED");
}

// Action que modifica o estado do campo email
export const modificaEmail = (email) => {
  return {
    type: MODIFICA_EMAIL,
    payload: email
  }
}

// Action que modifica o estado do campo senha
export const modificaSenha = (senha) => {
  return {
    type: MODIFICA_SENHA,
    payload: senha
  }
}

//Action para logar usuário
export const logarUsuario = ({ email, senha }) => {  

  return dispatch => {

    /*let db = openDatabase({name: 'medeibem.db', createFromLocation: "~me_dei_bem.db"});
    db.transaction((tx) => {
      tx.executeSql('SELECT * FROM USER', [], (tx, results) => {  
        //console.log(results.rows.item);        
        var len = results.rows.length;        
        for (let i = 0; i <= len; i++) {
          let row = results.rows.item(i);
          alert(`Record: ${row.ind_logado}`);          
        }
      });
    });*/

    
    axios.get(`${BASE_PROTOCOL}://${BASE_URL}/${BASE_API}?a=vl&d={%22email%22:%22${email}%22,%22senha%22:%22${senha}%22}`)
      .then(function(response) {

        console.log(`${response.data.nome}`)
        
        //const dadosUsuario = [response.data.email, response.data.nome, response.data.sobrenome, response.data.senha, response.data.radar_uf, response.data.radar_cid, response.data.cidade, response.data.app_key, response.data.sinc_stat, response.data.dth_last_sincr]
         //console.log("Dados Usuario ->" + typeof(JSON.stringify(dadosUsuario)) );
        
        switch (response.data.sinc_stat) {
          case 0:
            loginUsuarioErro(response.data, dispatch);
          case 1:
            let db = openDatabase({name: 'medeibem.db', createFromLocation: "~me_dei_bem.db"});

              db.transaction((tx) => {

                tx.executeSql('SELECT idUser, email, nome, sobrenome, senha, radar_uf, radar_cid, radar_cidade, token, ind_logado, dth_ult_sincr FROM USER', [], (tx, results) => {
                
                  switch (results.rows.length) {
                    case 0:
                      //let arrayDadosUsuario = [response.data.email, response.data.nome, response.data.sobrenome, response.data.senha, response.data.radar_uf, response.data.radar_cid, response.data.cidade, response.data.app_key, response.data.sinc_stat, response.data.dth_last_sincr]                    
                      //let dadosUsuario =  JSON.stringify(response.data.email, response.data.nome, response.data.sobrenome, response.data.radar_uf, response.data.radar_cid, response.data.cidade, response.data.app_key, response.data.sinc_stat, response.data.dth_last_sincr);
                      tx.executeSql(`INSERT INTO USER (email, nome, sobrenome, senha, radar_uf, radar_cid, radar_cidade, token, ind_logado, dth_ult_sincr) VALUES ("${email}", "${response.data.nome}", "${response.data.sobrenome}", "${senha}", "${response.data.radar_uf}", "${response.data.radar_cid}", "${response.data.cidade}", "${response.data.app_key}", "${response.data.sinc_stat}", "${response.data.dth_last_sincr}")`, successCB, errorCB);
                      //console.log("Executou a query, com esses dados ->" + dadosUsuario);
                    case 1:
                      loginUsuarioSucesso(response.data, dispatch);
                    break;
                  }

                });

              });        
          break;
        }
      
    });

    /*
      if (response.data.sinc_stat === 0) {
        loginUsuarioErro(response.data, dispatch);
      }
      else {
        let db = openDatabase({name: 'medeibem.db', createFromLocation: "~me_dei_bem.db"});
        db.transaction((tx) => {
          tx.executeSql('SELECT idUser, email, nome, sobrenome, senha, radar_uf, radar_cid, radar_cidade, token, ind_logado, dth_ult_sincr FROM USER', [], (tx, results) => { 
            //console.log(results);  
            
            if (results.rows.length === 0) {
              tx.executeSql('INSERT INTO USER (email, nome, sobrenome, senha, radar_uf, radar_cid, radar_cidade, token, ind_logado, dth_ult_sincr) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [response.data.email, response.data.nome, response.data.sobrenome, response.data.senha, response.data.radar_uf, response.data.radar_cid, response.data.cidade, response.data.app_key, response.data.radar_sinc_stat, response.data.dth_last_sincr], successCB, errorCB);
            }

            //let dados = results.rows.length              
            for (let i = 0; i <= dados; i++) {
              
              if (results.rows.length === 0) {
                tx.executeSql('INSERT INTO USER (email, nome, sobrenome, senha, radar_uf, radar_cid, radar_cidade, token, ind_logado, dth_ult_sincr) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [response.data.email, response.data.nome, response.data.sobrenome, response.data.senha, response.data.radar_uf, response.data.radar_cid, response.data.cidade, response.data.app_key, response.data.radar_sinc_stat, response.data.dth_last_sincr], successCB, errorCB);
              }
              let row = results.rows.item(i);
              if (row.ind_logado === 1) {
                //Actions.funcoes();
                console.log(`Record: ${row.nome}`);
              }        
            }              
          });
        })
        //loginUsuarioSucesso(response.data, dispatch);
      }*/ 
  } 
}

const loginUsuarioSucesso = (response, dispatch) => {

  dispatch({ type: 'Usuário logodado com sucesso. :D', payload: Actions.funcoes() });

}

const loginUsuarioErro = (response, dispatch) => {  
  
  dispatch({ type: LOGIN_ERRO, payload: alert(response.sinc_msg) }); 
}
