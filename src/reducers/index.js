/*
* Container com todos os Reducers do app.
*/
import { combineReducers } from 'redux';
import AutenticacaoReducer from './AutenticacaoReducer';
import CadastrarUsuarioReducer from './CadastrarUsuarioReducer';
import CadastrarClassificadosReducer from './CadastrarClassificadosReducer';

export default combineReducers({
  AutenticacaoReducer,
  CadastrarUsuarioReducer,
  CadastrarClassificadosReducer 
});
