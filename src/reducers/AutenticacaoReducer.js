/*
* Reducer responsavel por gerenciar os estados
* do processo de autenticação do usuario no app
*/

import { MODIFICA_EMAIL, MODIFICA_SENHA, LOGIN_ERRO } from '../actions/types';

// Estado inicial do processo de Login.
const INITIAL_STATE = {
  email: '',
  senha: '',  
  erroLogin: ''
}

// Alterações dos estados
export default (state = INITIAL_STATE, action) => {
  //console.log(state);
  switch (action.type) {
    case MODIFICA_EMAIL:
      return { ...state, email: action.payload }
    case MODIFICA_SENHA:
      return { ...state, senha: action.payload }
    case LOGIN_ERRO:
      return { ...state, erroLogin: action.payload }     
    default:
      return state;      
  }
}
