/*
* Reducer responsavel por gerenciar os estados
* do cadastro de classificados do app
*/

// Constantes do types das Actions
import {
  MODIFICA_SITUACAO,
  MODIFICA_OBSERVACAO,
  MODIFICA_CATEGORIA,
  MODIFICA_SUBCATEGORIA,
  MODIFICA_TITULO,
  MODIFICA_DESCRICAO,
  MODIFICA_HORARIO_INICIO_1,
  MODIFICA_HORARIO_FIM_1,
  MODIFICA_HORARIO_INICIO_2,
  MODIFICA_HORARIO_FIM_2,
  MODIFICA_TELEFONE,
  MODIFICA_EMAIL
} from '../actions/types';

// Estado inicial do cadastro de classificados.
const INITIAL_STATE = {
  situacaoClassificado: '',
  observacao: '',
  categoria: '',
  subcategoria: '',
  titulo: '',
  descricao: '',
  horarioInicio_1: '',
  horarioFim_1: '',
  horarioInicio_2: '',
  horarioFim_2: '',
  telefone: '',
  email: ''
}

// Alterações dos estados
export default (state = INITIAL_STATE, action) => {  
  switch (action.type) {
    case MODIFICA_SITUACAO:      
      return { ...state, situacaoClassificado: action.payload }; 
    case MODIFICA_OBSERVACAO:
      return { ...state, observacao: action.payload };
    case MODIFICA_CATEGORIA:    
      return { ...state, categoria: action.payload };
    case MODIFICA_SUBCATEGORIA:
      return { ...state, subcategoria: action.payload };
    case MODIFICA_TITULO:
      return { ...state, titulo: action.payload };
    case MODIFICA_DESCRICAO:
      return { ...state, descricao: action.payload };
    case MODIFICA_HORARIO_INICIO_1:
      return { ...state, horarioInicio_1: action.payload };
    case MODIFICA_HORARIO_FIM_1:
      return { ...state, horarioFim_1: action.payload };
    case MODIFICA_HORARIO_INICIO_2:
      return { ...state, horarioInicio_2: action.payload };
    case MODIFICA_HORARIO_FIM_2:
      return { ...state, horarioFim_2: action.payload };
    case MODIFICA_TELEFONE:
      return { ...state, telefone: action.payload };
    case MODIFICA_EMAIL:
      return { ...state, email: action.payload };
    default:
      return state;    
  }

}
  