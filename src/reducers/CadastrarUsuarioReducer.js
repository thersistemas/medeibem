/*
* Reducer responsavel por gerenciar os estados
* do processo de cadastrar usuario no app
*/

import { 
  MODIFICA_NOME,
  MODIFICA_SOBRENOME,
  MODIFICA_EMAIL,
  MODIFICA_RADAR_ESTADO,
  ERRO_LISTAR_ESTADO,
  MODIFICA_RADAR_CIDADE,
  MODIFICA_SENHA,
  SUCESSO_CADASTRO_USUARIO,
  ERRO_CADASTRO_USUARIO
} from '../actions/types';

// Estado inicial do processo de cadastrar.
const INITIAL_STATE = {
  nome: '',
  sobrenome: '',
  email: '',
  radarEstado: '',
  radarCidade: '',
  senha: '',
  sucessoCadastroUsuario: '',
  erroCadastroUsuario: ''
}

// Alterações dos estados
export default (state = INITIAL_STATE, action) => {
 // console.log(state);
  switch (action.type) {
    case MODIFICA_NOME:
      return { ...state, nome: action.payload }
    case MODIFICA_SOBRENOME:
      return { ...state, sobrenome: action.payload } 
    case MODIFICA_EMAIL:
      return { ...state, email: action.payload } 
    case MODIFICA_RADAR_ESTADO:
      return { ...state, radarEstado: action.payload }  
    case MODIFICA_RADAR_CIDADE:
      return { ...state, radarCidade: action.payload }
    case MODIFICA_SENHA:
      return { ...state, senha: action.payload } 
    case SUCESSO_CADASTRO_USUARIO:
      return { ...state, sucessoCadastroUsuario: action.payload }
    case ERRO_CADASTRO_USUARIO:
      return { ...state, erroCadastroUsuario: action.payload }       
    default:
      return state;
  }  
}
