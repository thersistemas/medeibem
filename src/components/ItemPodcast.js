/*
Classe do componente ItemPodcast.
*/

import React, { Component } from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';

import Pesquisar from './Pesquisar';

export default class ItemClassificados extends Component {    
  render() {   
    return (      
      <View>        
        <Pesquisar />
        <View style={styles.container}>
          <View style={styles.containerBotao}>
            <FontAwesome style={styles.botao}>{Icons.pause}</FontAwesome>
          </View>

          <View style={styles.containerTexto}>           
              <Text style={styles.txtData}>2018/FEV #02</Text>
              <Text style={styles.txtTempo}><FontAwesome>{Icons.headphones}</FontAwesome> 00:15:00     <FontAwesome>{Icons.handPeaceO}</FontAwesome> 4570</Text>           
          </View>
        </View>

        <View style={styles.container}>
          <View style={styles.containerBotao}>
            <FontAwesome style={styles.botao}>{Icons.play}</FontAwesome>
          </View>
          
          <View style={styles.containerTexto}>           
              <Text style={styles.txtData}>2018/FEV #02</Text>
              <Text style={styles.txtTempo}><FontAwesome>{Icons.headphones}</FontAwesome> 00:15:00     <FontAwesome>{Icons.handPeaceO}</FontAwesome> 4570</Text>           
          </View>
        </View>

        <View style={styles.container}>
          <View style={styles.containerBotao}>
            <FontAwesome style={styles.botao}>{Icons.play}</FontAwesome>
          </View>
          
          <View style={styles.containerTexto}>           
              <Text style={styles.txtData}>2018/FEV #02</Text>
              <Text style={styles.txtTempo}><FontAwesome>{Icons.headphones}</FontAwesome> 00:15:00     <FontAwesome>{Icons.handPeaceO}</FontAwesome> 4570</Text>           
          </View>
        </View>

        <View style={styles.container}>
          <View style={styles.containerBotao}>
            <FontAwesome style={styles.botao}>{Icons.play}</FontAwesome>
          </View>
          
          <View style={styles.containerTexto}>           
              <Text style={styles.txtData}>2018/FEV #02</Text>
              <Text style={styles.txtTempo}><FontAwesome>{Icons.headphones}</FontAwesome> 00:15:00     <FontAwesome>{Icons.handPeaceO}</FontAwesome> 4570</Text>           
          </View>
        </View>
      </View>      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#ff7c11',
    margin: 10,
    alignItems: 'center'
  },
  containerBotao:
  {
    backgroundColor: '#FFF',           
    alignItems: 'flex-start',
    marginLeft: 15,
    marginRight: 5
  },
  botao:
  {
    fontSize: 80,
    color: '#ff7c11'    
  },
  containerTexto:
  {
    flexDirection: 'column',    
    marginLeft: 5   
  },
  txtData: {        
    fontSize: 40,
    color: '#ff7c11' 
  },
  txtTempo:
  {
   fontSize: 18,
   marginBottom: 15
  },  
});
