import React, { Component } from 'react';
import { Button, Image, Picker, ScrollView, StatusBar, StyleSheet, Text, TextInput, TouchableHighlight, View } from 'react-native';
//import { Actions } from 'react-native-router-flux';
//import { connect } from 'react-redux';

//import { enviaCodigoQR } from '../actions/DescontoActions';

export default class FormCadastroUsuario extends Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <StatusBar backgroundColor='#5258aa' />

          <View style={styles.containerLogo}>
            <Image source={require('../imgs/logo.png')} />
            <Text style={{ fontSize: 30, color: '#5258aa' }}>Baixa de Desconto</Text>
          </View>

          <View style={styles.containerBotoes}>
            <TouchableHighlight style={styles.btnLerQR} underlayColor={'#FFDD8A'} onPress={() => false}>
              <Text style={styles.textoBotao}>Ler Código QR</Text>
            </TouchableHighlight>
          </View>       

          <View style={styles.containerCampos}>
            <View style={{ marginBottom: 15 }}>
              <TextInput placeholder="Código de Desconto"
                style={{ fontSize: 20, backgroundColor: '#fff' }}
                value={this.props.codigoQR}
                onChangeText={codigoQR => this.props.enviaCodigoQR(codigoQR)}                             
              />
            </View>
            <View style={{ marginBottom: 15 }}>
              <TextInput placeholder="Senha Vendedor" style={{ fontSize: 20, backgroundColor: '#fff' }} />
            </View>
            <View style={{ marginBottom: 15 }}>
              <TextInput placeholder="Quantidade Vendida" style={{ fontSize: 20, backgroundColor: '#fff' }} />
            </View>    
          </View>

          <View style={styles.containerBotoes}>
            <TouchableHighlight style={styles.btnRegistrar} underlayColor={'#d0f6ff'} onPress={() =>false}>
              <Text style={styles.textoBotao}>Registrar</Text>
            </TouchableHighlight>
          </View>

        </View>
      </ScrollView>
    );
  }
}

/*const mapStateToProps = state => (
  {
      codigoQR: state.DescontoReducer.codigoQR
  }
)*/

//export default connect(mapStateToProps, { enviaCodigoQR })(FormCadastroUsuario)

const styles = StyleSheet.create({
  container:
  {
    flex: 1,
    padding: 10
  },
  containerLogo:
  {
    marginTop: 30,    
    alignItems: 'center',
    flex: 1    
  },
  containerCampos:
  {
    flex: 2,
    marginTop: 30
  },
  containerBotoes:
  {
    flex: 2,
    marginTop: 15
  },
  btnRegistrar:
  {
    alignItems: 'center',
    backgroundColor: '#b1f1ff',
    borderWidth: 1,
    borderColor: '#000',
    padding: 10
  },
  btnLerQR:
  {
    alignItems: 'center',
    backgroundColor: '#ffd56d',
    borderWidth: 1,
    borderColor: '#000',
    padding: 10    
  },
  textoBotao:
  {
    fontSize: 20,
    color: '#3f3315'
  }
});
