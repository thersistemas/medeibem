import React, { Component } from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import QRCode from 'react-native-qrcode';

import { BASE_PROTOCOL } from '../services/UrlApi';

export default class ItemDetalhePromocao extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.txtTitulo}>{this.props.item.titulo}</Text>

        <View style={styles.containerImagem}>
          <ScrollView horizontal={true}>
            <Image style={styles.imagem} source={{ uri: `${BASE_PROTOCOL}` + this.props.item.img_link1 }} />
            <Image style={styles.imagem} source={{ uri: `${BASE_PROTOCOL}` + this.props.item.img_link2 }} />
            <Image style={styles.imagem} source={{ uri: `${BASE_PROTOCOL}` + this.props.item.img_link3 }} />
            <Image style={styles.imagem} source={{ uri: `${BASE_PROTOCOL}` + this.props.item.img_link4 }} />
            <Image style={styles.imagem} source={{ uri: `${BASE_PROTOCOL}` + this.props.item.img_link5 }} />
            <Image style={styles.imagem} source={{ uri: `${BASE_PROTOCOL}` + this.props.item.img_link6 }} />
          </ScrollView>
        </View>

        <View>
          <Text style={styles.txtDescricaoProduto}>{this.props.item.texto}</Text>
        </View>

        <View style={styles.containerDetalheDesconto}>
          <View style={styles.detalheDesconto}>
            <Text style={styles.labels}>De:  </Text>
            <Text style={styles.labels}>Por:  </Text>
            <Text style={styles.labels}>Desconto:  </Text>
            <Text style={styles.labels}>Validade Até:  </Text>
            <Text style={styles.labels}>Quantidade:  </Text>
          </View>

          <View style={styles.detalheDesconto}>
            <Text style={styles.valores}>R${this.props.item.preco_original}</Text>
            <Text style={styles.valores}>R${this.props.item.preco_promocao}</Text>
            <Text style={styles.valores}>{this.props.item.desconto}</Text>
            <Text style={styles.valores}>{this.props.item.validade}</Text>
            <Text style={styles.valores}>{this.props.item.qtde_ofertada} / {this.props.item.qtde_disponivel}</Text>
          </View>
        </View>

        <View style={styles.constainerCodigoDesconto}>
          <Text style={styles.labels}>Código de Desconto:</Text>
          <Text style={styles.txtCodigoDesconto}>{this.props.item.codigo}</Text>
        </View>

        <View style={styles.containerQRCode}>
          <QRCode value={this.props.item.codigo} size={200} bgColor='#000' fgColor='#fff' />
        </View>

        <View style={styles.containerVendedor}>
          <Text style={styles.labels}>Vendedor:</Text>
          <Text style={styles.valores}>{this.props.item.vnd_empresa}</Text>
          <Text style={styles.valores}>{this.props.item.vnd_email}</Text>
          <Text style={styles.valores}>{this.props.item.vnd_fone1}</Text>
          <Text style={styles.valores}>{this.props.item.vnd_fone2}</Text>
          <Text style={styles.valores}>{this.props.item.vnd_end1}</Text>
          <Text style={styles.valores}>{this.props.item.vnd_end2}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    alignItems: 'center'
  },
  txtTitulo:
    {
      fontSize: 18,
      fontWeight: 'bold',
      textAlign: 'center',
      color: '#494949',
      marginTop: 20,
      marginBottom: 5
    },
  containerImagem: {
    width: 350,
    height: 300,
    marginBottom: 15
  },
  imagem:
    {
      width: 300,
      height: 300
    },
  txtDescricaoProduto:
    {
      fontSize: 18,
      textAlign: 'center',
      fontWeight: 'bold',
      color: '#494949',
      marginTop: 15,
      marginLeft: 30,
      marginRight: 30
    },
  containerDetalheDesconto:
    {
      flexDirection: 'row',
      marginTop: 30,
      marginBottom: 15,
      borderColor: '#49c1ff'
    },
  detalheDesconto:
    {
      flexDirection: 'column'
    },
  labels:
    {
      fontSize: 18,
      textAlign: 'right',
      fontWeight: 'bold',
      color: '#494949'
    },
  valores:
    {
      fontSize: 18,
      fontWeight: 'bold',
      color: '#498fff'
    },
  constainerCodigoDesconto:
    {
      marginTop: 15,
      marginBottom: 15,
      alignItems: 'center'
    },
  txtCodigoDesconto:
    {
      fontSize: 36,
      fontWeight: 'bold',
      color: '#a100af'
    },
  containerQRCode:
    {
      marginTop: 15,
      marginBottom: 15
    },
  containerVendedor:
    {
      alignItems: 'center',
      marginTop: 15,
      marginBottom: 10
    }
});
