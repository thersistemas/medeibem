import React, { Component } from 'react';
import { ScrollView, StatusBar, StyleSheet, Text, View } from 'react-native';

export default class PoliticaPrivacidade extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.texto}>
          Politíca de Privacidade
        </Text>

        <Text style={styles.texto}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus rutrum risus lorem, eget laoreet mi tempor eget. Proin iaculis sed mauris eu vestibulum. Ut id sollicitudin tortor. Curabitur fermentum in justo nec porttitor. Nam mollis at enim et consequat. Ut eget sollicitudin tellus, ac sodales enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
        </Text>

        <Text style={styles.texto}>
          Suspendisse scelerisque purus non malesuada cursus. Mauris ut ex consequat arcu lobortis aliquam. Nullam at massa purus. Aenean venenatis est turpis, non tincidunt tellus ullamcorper vel. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Interdum et malesuada fames ac ante ipsum primis in faucibus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam accumsan ipsum urna, sed sodales orci varius non. Quisque eu convallis turpis. Sed tempor dolor in porta imperdiet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam rhoncus nisi ut arcu congue, vel tristique libero sollicitudin. Nulla at dolor lacus. Aenean id pellentesque nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:
  {
    padding: 10
  },
  texto:
  {
    textAlign: 'left',
    marginBottom: 10,
    fontSize: 16,
    color: '#000000'
  }  
});
