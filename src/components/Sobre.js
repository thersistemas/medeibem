import React, { Component } from 'react';
import { Image, ScrollView, StatusBar, StyleSheet, Text, TouchableHighlight, View } from 'react-native';
import { Actions } from 'react-native-router-flux';

const logo = require('../imgs/logo.png');

export default class Sobre extends Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.logo}>
            <Image source={logo} />
            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#5258aa', textAlign: 'center' }}>
              Oportunidades, Descontos,
            </Text>
            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#5258aa', textAlign: 'center' }}>
              Promoções, Classificados e Podcasts
            </Text>
            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#5258aa', textAlign: 'center' }}>
              da sua cidade na tela do seu smartphone.
            </Text>
          </View>

          <View style={styles.containerContato}>
            <Text style={styles.texto}>
              contato@medeibem.mobi
            </Text>
            <Text style={styles.texto}>
              www.medeibem.mobi
            </Text>
            <Text style={styles.texto}>
              31-3885-2424
            </Text>
            <Text style={styles.texto}>
              Av Oraida Mendes de Castro, 6000, LJ 23
            </Text>
            <Text style={styles.texto}>
              Novo Silvestre - Viçosa - MG
            </Text>
          </View>

          <View style={styles.containerBotoes}>
            <TouchableHighlight style={styles.botao} underlayColor={'#d0f6ff'} onPress={() => { Actions.politicaPrivacidade(); }}>
              <Text style={styles.textoBotao}>Politíca de Privacidade</Text>
            </TouchableHighlight>
          </View>

          <View style={styles.containerRodape}>
            <Text style={styles.textoRodape}>2018 - Ther Sistemas Inovadores</Text>
            <Text style={styles.textoRodape}>www.medeibem.mobi</Text>
            <Text style={styles.textoRodape}>Todos os Direitos Reservados</Text>
          </View>

        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container:
    {
      padding: 10,
      flex: 1
    },
  logo:
    {
      marginTop: 10,
      alignItems: 'center',
      flex: 1
    },
  containerContato:
    {
      flex: 1,
      marginTop: 20,
    },
  containerBotoes:
    {
      flex: 1,
      marginTop: 10
    },
  containerRodape:
    {
      flex: 1
    },
  texto:
    {
      fontSize: 16,
      fontWeight: 'bold',
      color: '#5258aa',
      textAlign: 'center',
      marginLeft: 20,
      marginRight: 20
    },
  textoRodape:
    {
      fontSize: 16,
      textAlign: 'center',
      color: '#000000',
      marginLeft: 20,
      marginRight: 20
    },
  botao:
    {
      alignItems: 'center',
      backgroundColor: '#b1f1ff',
      padding: 10,
      marginBottom: 10
    },
  textoBotao:
    {
      fontSize: 20,
      color: '#0057ff'
    }
});
