import React, { Component } from 'react';
import { Image, ScrollView, StatusBar, StyleSheet, Text, TouchableHighlight, View } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Actions } from 'react-native-router-flux';

const logo = require('../imgs/logo_funcoes.png');

export default class Funcoes extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor='#5258aa' />
        <ScrollView>
        <View style={styles.logo}>
          <Image source={logo} />
        </View>

        <View style={styles.containerBotoes}>
          <Text style={{ fontSize: 16, color: '#000000' }}><FontAwesome style={{ fontSize: 18 }}>{Icons.userCircle}</FontAwesome> blabla@gmail.com</Text>
          <Text style={{ fontSize: 16, color: '#000000', marginBottom: 10 }}><FontAwesome style={{ fontSize: 18 }}>{Icons.eye}</FontAwesome> MG / VICOSA</Text>

          <TouchableHighlight style={styles.botao} underlayColor={'#FFDD8A'} onPress={() => { Actions.listaPromocoes(); }}>
            <Text style={styles.textoBotao}><FontAwesome style={{ fontSize: 24 }}>{Icons.ticket}</FontAwesome> Promoções</Text>
          </TouchableHighlight>
          
          <TouchableHighlight style={styles.botao} underlayColor={'#FFDD8A'} onPress={() => { Actions.ListaClassificados(); }}>
            <Text style={styles.textoBotao}><FontAwesome style={{ fontSize: 24 }}>{Icons.handshakeO}</FontAwesome> Classificados</Text>
          </TouchableHighlight>

          <TouchableHighlight style={styles.botao} underlayColor={'#FFDD8A'} onPress={() => { Actions.ListaPodcast(); }}>
            <Text style={styles.textoBotao}><FontAwesome style={{ fontSize: 24 }}>{Icons.volumeUp}</FontAwesome> Podcasts</Text>
          </TouchableHighlight>
        </View>        

        <View style={styles.containerRodape}>
          <Text style={styles.textoRodape}>2018 - Ther Sistemas Inovadores</Text>
          <Text style={styles.textoRodape}>www.medeibe.mobi</Text>
          <Text style={styles.textoRodape}>Todos os Direitos Reservados</Text>
        </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:
  {
    padding: 10,
    flex: 1
  },
  logo:
  {
    flex: 1,
    marginTop: 10
  },  
  containerBotoes:
  {
    flex: 1,
    marginTop: 15,
    marginBottom: 15    
  },
  textoEmail:
  {
    fontSize: 20,
    marginBottom: 10
  },
  botao:
  {
    alignItems: 'center',
    backgroundColor: '#ffd56d',
    padding: 10,
    marginBottom: 10,
  },
  textoBotao:
  {
    fontSize: 20,
    color: '#3f3315'
  },
  textoRodape:
  {
    fontSize: 16,        
    textAlign: 'center',
    color: '#000000',
    marginLeft: 20,
    marginRight: 20
  },
  containerRodape:
  {
    flex: 1
  }
});