import React, { Component } from 'react';
import { ScrollView, StatusBar, StyleSheet, TextInput, View } from 'react-native';

import Pesquisar from './Pesquisar';
import ListaClassificados from '../services/ListaClassificados';

export default class ViewListaPromocoes extends Component {
  render() {
    return (
      <View>
        <Pesquisar />
        <ListaClassificados />
      </View>
    );
  }
}
