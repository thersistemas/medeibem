import React, { Component } from 'react';
import { ScrollView, StatusBar, StyleSheet, TextInput, View } from 'react-native';

import Pesquisar from './Pesquisar';
import ListaPromocao from '../services/ListaPromocao';

export default class ViewListaPromocoes extends Component {
  render() {
    return (
      <View>
        <Pesquisar />
        <ListaPromocao />
      </View>
    );
  }
}
