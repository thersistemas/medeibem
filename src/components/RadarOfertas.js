import React, { Component } from 'react';
import { Button, Image, Picker, StatusBar, StyleSheet, Text, TouchableHighlight, View } from 'react-native';

export default class RadarOfertas extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor='#5258aa' />

        <View style={styles.containerLogo}>
          <Image source={require('../imgs/logo.png')} />
          <Text style={{ fontSize: 30, color: '#5258aa' }}>Radar de Ofertas</Text>
        </View>

        <View style={styles.containerPicker}>
          <Text style={{ fontSize: 20 }}>Radar-Estado</Text>
          <View style={{ backgroundColor: '#fff', marginBottom: 10 }}>
            <View style={styles.picker}>
              <Picker>
                <Picker.Item label="Minas-Gerais" value="MG" />
              </Picker>
            </View>
          </View>
          <Text style={{ fontSize: 20, marginTop: 10 }}>Radar-Cidade</Text>
          <View style={{ backgroundColor: '#fff', marginBottom: 20 }}>
            <View style={styles.picker}>
              <Picker >
                <Picker.Item label="Viçosa" value="Vicosa" />
              </Picker>
            </View>
          </View>
        </View>

        <View style={styles.containerBotoes}>
          <TouchableHighlight style={styles.botao} underlayColor={'#d0f6ff'} onPress={() => false}>
            <Text style={styles.textoBotao}>Salvar</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:
    {
      flex: 1,
      padding: 10
    },
  containerLogo:
    {
      flex: 4,
      marginTop: 15,
      alignItems: 'center'
    },
  containerPicker:
    {
      flex: 3
    },
  picker:
    {
      paddingLeft: 10,
      backgroundColor: '#ffffff'
    },
  containerBotoes:
    {
      flex: 1,
      marginTop: 20
    },
  botao:
    {
      alignItems: 'center',
      backgroundColor: '#b1f1ff',
      padding: 10,
      marginBottom: 10,
      borderWidth: 1
    },
  textoBotao:
    {
      fontSize: 20,
      color: '#3f3315'
    }
});
