import React, { Component } from 'react';
import { Button, Image, ScrollView, StatusBar, StyleSheet, Text, TextInput, TouchableHighlight, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { modificaEmail, modificaSenha, logarUsuario } from '../actions/AutenticacaoActions';
//import { conexao } from '../db/dbi';

class FormLogin extends Component {
  
  _logarUsuario() {

    const { email, senha } = this.props;

    this.props.logarUsuario({ email, senha });
    //this.props.verificaUsuario();
    //conexao();
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <StatusBar backgroundColor='#5258aa' />

          <View style={styles.containerLogo}>
            <Image source={require('../imgs/logo.png')} />
            <Text style={{ fontSize: 30, color: '#5258aa' }}>Login</Text>
          </View>

          <View style={styles.containerCampos}>
            <TextInput
              style={{ fontSize: 20 }}
              placeholder="E-mail"
              value={this.props.email}
              onChangeText={email => this.props.modificaEmail(email)}
            />
            <TextInput style={{ fontSize: 20 }}
              placeholder="Senha"
              value={this.props.senha}
              onChangeText={senha => this.props.modificaSenha(senha)}
            />                     
          </View>

          <View style={styles.containerBotoes}>
            <TouchableHighlight style={styles.botao} underlayColor={'#FFDD8A'} onPress={() => this._logarUsuario()}>
              <Text style={styles.textoBotao}>Entrar</Text>              
            </TouchableHighlight>

            <TouchableHighlight style={styles.botao} underlayColor={'#FFDD8A'} onPress={() => { Actions.formCadastro(); }}>
              <Text style={styles.textoBotao}>Quero Participar!</Text>
            </TouchableHighlight>

            <TouchableHighlight style={styles.botao} underlayColor={'#FFDD8A'} onPress={() => false}>
              <Text style={styles.textoBotao}>Puts, Esquici a Senha!</Text>
            </TouchableHighlight>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  email: state.AutenticacaoReducer.email,
  senha: state.AutenticacaoReducer.senha,
  erroLogin: state.AutenticacaoReducer.erroLogin
})

export default connect(mapStateToProps, { modificaEmail, modificaSenha, logarUsuario })(FormLogin);

const styles = StyleSheet.create({
  container:
    {
      flex: 1,
      padding: 10
    },
  containerLogo:
    {
      flex: 1,
      marginTop: 20,
      alignItems: 'center'
    },
  containerCampos:
    {
      flex: 1,
      marginTop: 30
    },
  containerBotoes:
    {
      flex: 1,
      marginTop: 30
    },
  botao:
    {
      alignItems: 'center',
      backgroundColor: '#ffd56d',
      padding: 10,
      marginBottom: 10,
    },
  textoBotao:
    {
      fontSize: 20,
      color: '#3f3315'
    }
});