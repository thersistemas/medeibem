/*
* Classe do componente ItemPromocao.
*/

import React, { Component } from 'react';
import { Image, StyleSheet, Text, TouchableHighlight, View } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { BASE_PROTOCOL } from '../services/UrlApi';

export default class ItemPromocao extends Component {    
  render() {   
    return (      
      <View style={styles.item}>               
        <Text style={styles.txtTitulo}>{this.props.item.titulo}</Text>
        <View style={styles.containerImagem}>
          <TouchableHighlight onPress={() => { Actions.detalhePromocao(); }}>
            <Image style={styles.imagem} source={{ uri: `${BASE_PROTOCOL}` + this.props.item.img_link1 }} />
          </TouchableHighlight>
        </View>
        <Text style={styles.txtValor}>De R${this.props.item.preco_original} por R$<Text style={{ fontSize: 20 }}>{this.props.item.preco_promocao}</Text></Text>          
        <Text style={styles.txtValor}><Text style={{ fontSize: 20 }}>{this.props.item.desconto}</Text> de desconto!</Text>        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#49c1ff',
    margin: 10,
    padding: 10,
    alignItems: 'center'
  },
  containerImagem: {        
    width: 250,
    height: 250,    
  },
  imagem:
  {
    width: 250,
    height: 250
  },
  detalhesItem:
  {
    marginLeft: 20,
    flex: 1
  },
  txtTitulo:
  {
    fontSize: 20,
    fontWeight: 'bold',    
    textAlign: 'center',
    color: '#494949',
    marginBottom: 5
  },
  txtValor:
  {
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#5a97ff'
  },
  txtDetalhes:
  {
    fontSize: 16
  }
});