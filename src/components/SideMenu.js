import React, { Component } from 'react';
import { StyleSheet, Text, TouchableHighlight, View } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Actions } from 'react-native-router-flux';

export default class SideMenu extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerBotao}>
          <TouchableHighlight>
            <Text style={styles.tituloMenu}>Configuração</Text>
          </TouchableHighlight>
        </View >
        <View style={styles.containerBotao}>        
          <TouchableHighlight underlayColor={'#5258aa'} activeOpacity={0.3} onPress={() => { Actions.formBaixaDesconto(); }}>          
            <Text style={styles.txtMenu}><FontAwesome>{Icons.checkSquareO}</FontAwesome>  Baixa de Desconto</Text>
          </TouchableHighlight>
        </View >
        <View style={styles.containerBotao}>
          <TouchableHighlight underlayColor={'#5258aa'} activeOpacity={0.3} onPress={() => { Actions.formCadastroClassificados(); }}>
            <Text style={styles.txtMenu}><FontAwesome>{Icons.handshakeO}</FontAwesome>  Meu Classificado</Text>
          </TouchableHighlight>
        </View >
        <View style={styles.containerBotao}>
          <TouchableHighlight underlayColor={'#5258aa'} activeOpacity={0.3} onPress={() => { Actions.formCadastro(); }} >
            <Text style={styles.txtMenu}><FontAwesome>{Icons.addressCardO}</FontAwesome>  Meu Cadastro</Text>
          </TouchableHighlight>
        </View >
        <View style={styles.containerBotao}>
          <TouchableHighlight underlayColor={'#5258aa'} activeOpacity={0.3} onPress={() =>{ Actions.radarOfertas(); }}>
            <Text style={styles.txtMenu}><FontAwesome>{Icons.eye}</FontAwesome>  Radar de Ofertas</Text>
          </TouchableHighlight>
        </View >
        <View style={styles.containerBotao}>
          <TouchableHighlight underlayColor={'#5258aa'} activeOpacity={0.3} onPress={() => { Actions.sobre(); }}>
            <Text style={styles.txtMenu}><FontAwesome>{Icons.infoCircle}</FontAwesome>  Sobre</Text>
          </TouchableHighlight>
        </View >
        <View style={styles.containerBotao}>
          <TouchableHighlight underlayColor={'#5258aa'} activeOpacity={0.3} onPress={() => { Actions.formLogin(); }}>
            <Text style={styles.txtMenu}><FontAwesome>{Icons.signOut}</FontAwesome>  Sair</Text>
          </TouchableHighlight>
        </View >            
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:
  {
    flex: 1,
    backgroundColor: '#5258aa'       
  },
  tituloMenu:
  {
    fontSize: 26,    
    textAlign: 'center',
    color: '#fff',
    marginBottom: 5
  },
  containerBotao:
  {
    borderBottomWidth: 1,
    borderColor: '#fff',
    marginTop: 5,
    marginRight: 15,
    marginLeft: 15,    
    paddingTop: 15   
  },
  txtMenu:
  {
    fontSize: 20,    
    color: '#fff',
    marginLeft: 15,
    marginBottom: 5
    //'#646abc'
  }
});