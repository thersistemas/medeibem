import React, { Component } from 'react';
import { Buttom, Picker, ScrollView, StatusBar, StyleSheet, Text, TextInput, TouchableHighlight, View } from 'react-native';
import { connect } from 'react-redux';
import axios from 'axios';
import _ from 'lodash';
import { openDatabase } from "react-native-sqlite-storage";

import { BASE_PROTOCOL, BASE_URL, BASE_API } from '../services/UrlApi';
import {   
  modificaCategoria,
  modificaSubcategoria,
  modificaTitulo,
  modificaDescricao,
  modificaHorarioInicio_1,
  modificaHorarioFim_1,
  modificaHorarioInicio_2,
  modificaHorarioFim_2,
  modificaTelefone,
  modificaEmail,
  cadastraClassificado
} from '../actions/CadastrarClassificadosActions';

class FormCadastroClassificados extends Component {
  
  constructor(props) {

    super(props);

    this.state = { 
      listaCategorias: [], 
      listaSubcategorias: [],
      situacaoClassificado: '',
      observacao: '',
      appKey: '' 
    };
    
  }
  
  _cadastraClassificado() {       

    let db = openDatabase({name: 'medeibem.db', createFromLocation: "~me_dei_bem.db"});  

    var appKey = '';
  
    db.transaction((tx) => {

      tx.executeSql('SELECT token FROM USER LIMIT 1', [], (tx, results) => {

        appKey = results.rows.item(0).token;
        
        const { categoria, subcategoria, titulo, descricao, horarioInicio_1, horarioFim_1, horarioInicio_2, horarioFim_2, telefone, email } = this.props;
  
        this.props.cadastraClassificado({ appKey, categoria, subcategoria, titulo, descricao, horarioInicio_1, horarioFim_1, horarioInicio_2, horarioFim_2, telefone, email });
      
      });
      
    });    
    
  }  
  
  componentDidMount() {    

    // Get de lista de categorias na API.
    axios.get(`${BASE_PROTOCOL}://${BASE_URL}/${BASE_API}?a=lctg&k=${this.props.appKey}`)
      .then(response => { this.setState({ listaCategorias: response.data }); })
      .catch(() => { console.log('Erro ao recuperar os dodos!') });


    // Verifica se existe registro de  classificados na base local
    // -- Inicio --
    let db = openDatabase({name: 'medeibem.db', createFromLocation: "~me_dei_bem.db"});
    db.transaction((tx) => {
      tx.executeSql('SELECT idCLASSIF, titulo, texto, contato_horario1, contato_horario2, contato_tel, contato_email, data_cad, data_exp, situacao, obs, last_update, ind_sinc FROM CLASSIF', [], (tx, results) => {

        switch (results.rows.length) {
          case 0:
            this.setState({ situacaoClassificado: "Disponível" });
            this.setState({ observacao: "Apenas 1 anúncio por vez. Duração de 7 dias. Renove sempre que desejar." });
          break;
          case 1:
            axios.get(`${BASE_PROTOCOL}://${BASE_URL}/${BASE_API}?a=cs&k=${this.props.appKey}`)            
              .then(response => { 
                this.setState({ situacaoClassificado: response.data.situacao });
                this.setState({ observacao: response.data.obs });
              })
              .catch((erro) => { Alert(erro); });
          break;                    
        }

      });

    });
    // -- Fim --
    
  }  

  onValueChangeCategoria(idCategoria) {

    this.props.modificaCategoria(idCategoria);

    if (idCategoria == '') {
      return this.setState({ listaSubcategorias: [] });
    }
    else {
      axios.get(`${BASE_PROTOCOL}://${BASE_URL}/${BASE_API}?a=lsctg&k=D4U7X6O5K5O5Z5O0D9I4Z3U5Z3O4Y1E21518217746&ctg=${idCategoria}`)
        .then(response => { this.setState({ listaSubcategorias: _.values(response.data) }); })
        .catch((erro) => { console.log('Erro ao recuperar lista de subcategoria!' + erro) });
    } 

  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <StatusBar backgroundColor='#5258aa' />
          <View style={styles.containerTexto}>
            <View style={styles.containerLabel}>
              <Text style={styles.lblSituacao}>Situação:  </Text>
              <Text style={styles.lblObs}>Obs.:  </Text>
            </View>            

            <View style={styles.containerTextoLabel}>
              <View style={{ marginBottom: 10 }}>
                <Text style={styles.texto}>{this.state.situacaoClassificado}</Text>                
              </View>
              <View style={{ width: 280}}>
                <Text style={styles.texto}>{this.state.observacao}</Text>
              </View>              
            </View>
          </View>

          <View style={styles.containerFormulario}>
            <Text style={styles.txtLabel}>Categoria:</Text> 
            <View style={{ backgroundColor: '#fff', marginBottom: 20 }}>
                      
              <Picker selectedValue={this.props.categoria} onValueChange={this.onValueChangeCategoria.bind(this)}>
                <Picker.Item label="" value="" />
                               
                {this.state.listaCategorias.map((i, index) => (
                  <Picker.Item key={index} label={i.categoria} value={i.idcategoria} />                 
                ))}              
              </Picker>
            </View>
            <Text style={styles.txtLabel}>Subcategoria:</Text>
            <View style={{ backgroundColor: '#fff' }}>           
              <Picker 
                selectedValue={this.props.subcategoria} 
                onValueChange={subcategoria => this.props.modificaSubcategoria(subcategoria)}                
              >                
                <Picker.Item label="Selecione uma Categoria..." value="" />
                {this.state.listaSubcategorias.map((i, index) => (
                  <Picker.Item key={index} label={i.subcategoria} value={i.idsubcategoria } />
                  ))
                }
              </Picker>             
            </View>
            <TextInput
              style={styles.textInput}
              placeholder="Título (40 caracteres)"
              value={this.props.titulo}
              onChangeText={titulo => this.props.modificaTitulo(titulo)}
            />

            <View style={styles.containerDescricao}>
              <Text style={styles.txtLabel}>Descrição: (120 caracteres)</Text>
              <View style={styles.descricao}>
                <TextInput
                  multiline={true}
                  editable={true}
                  numberOfLines={5}
                  maxLength={120}
                  underlineColorAndroid='transparent'
                  value={this.props.descricao}
                  onChangeText={descricao => this.props.modificaDescricao(descricao)}
                />
              </View>
            </View>

            <Text style={styles.txtLabel}>Contato - Horário Disponível #1</Text>
            <View style={styles.containerPicker}>
              <View style={{ flexDirection: 'row'}}>              
                <View style={styles.picker}>             
                  <Picker selectedValue={this.props.horarioInicio_1} onValueChange={horarioInicio_1 => this.props.modificaHorarioInicio_1(horarioInicio_1)}>
                    <Picker.Item label="" value=""/>
                    <Picker.Item label="00:00" value="00:00" />
                    <Picker.Item label="01:00" value="01:00" />
                    <Picker.Item label="02:00" value="02:00" />
                    <Picker.Item label="03:00" value="03:00" />
                    <Picker.Item label="04:00" value="04:00" />
                    <Picker.Item label="05:00" value="05:00" />
                    <Picker.Item label="06:00" value="06:00" />
                    <Picker.Item label="07:00" value="07:00" />
                    <Picker.Item label="08:00" value="08:00" />
                    <Picker.Item label="09:00" value="09:00" />
                    <Picker.Item label="10:00" value="10:00" />
                    <Picker.Item label="11:00" value="11:00" />
                    <Picker.Item label="12:00" value="12:00" />
                    <Picker.Item label="13:00" value="13:00" />
                    <Picker.Item label="14:00" value="14:00" />
                    <Picker.Item label="15:00" value="15:00" />
                    <Picker.Item label="16:00" value="16:00" />
                    <Picker.Item label="17:00" value="17:00" />
                    <Picker.Item label="18:00" value="18:00" />
                    <Picker.Item label="19:00" value="19:00" />
                    <Picker.Item label="20:00" value="20:00" />
                    <Picker.Item label="21:00" value="21:00" />
                    <Picker.Item label="22:00" value="22:00" />
                    <Picker.Item label="23:00" value="23:00" />                         
                  </Picker>                               
                </View>               
              </View>
           
              <View style={{ flexDirection: 'row' }}>             
                <View style={styles.picker}>             
                  <Picker selectedValue={this.props.horarioFim_1} onValueChange={horarioFim_1 => this.props.modificaHorarioFim_1(horarioFim_1)}>                  
                    <Picker.Item label="" value=""/>
                    <Picker.Item label="00:00" value="00:00" />
                    <Picker.Item label="01:00" value="01:00" />
                    <Picker.Item label="02:00" value="02:00" />
                    <Picker.Item label="03:00" value="03:00" />
                    <Picker.Item label="04:00" value="04:00" />
                    <Picker.Item label="05:00" value="05:00" />
                    <Picker.Item label="06:00" value="06:00" />
                    <Picker.Item label="07:00" value="07:00" />
                    <Picker.Item label="08:00" value="08:00" />
                    <Picker.Item label="09:00" value="09:00" />
                    <Picker.Item label="10:00" value="10:00" />
                    <Picker.Item label="11:00" value="11:00" />
                    <Picker.Item label="12:00" value="12:00" />
                    <Picker.Item label="13:00" value="13:00" />
                    <Picker.Item label="14:00" value="14:00" />
                    <Picker.Item label="15:00" value="15:00" />
                    <Picker.Item label="16:00" value="16:00" />
                    <Picker.Item label="17:00" value="17:00" />
                    <Picker.Item label="18:00" value="18:00" />
                    <Picker.Item label="19:00" value="19:00" />
                    <Picker.Item label="20:00" value="20:00" />
                    <Picker.Item label="21:00" value="21:00" />
                    <Picker.Item label="22:00" value="22:00" />
                    <Picker.Item label="23:00" value="23:00" />                         
                  </Picker>                               
                </View>                
              </View>
            </View>

            <Text style={styles.txtLabel}>Contato - Horário Disponível #2</Text>
            <View style={styles.containerPicker}>
              <View style={{ flexDirection: 'row' }}>              
                <View style={styles.picker}>             
                  <Picker selectedValue={this.props.horarioInicio_2} onValueChange={horarioInicio_2 => this.props.modificaHorarioInicio_2(horarioInicio_2)}>
                    <Picker.Item label="" value=""/>
                    <Picker.Item label="00:00" value="00:00" />
                    <Picker.Item label="01:00" value="01:00" />
                    <Picker.Item label="02:00" value="02:00" />
                    <Picker.Item label="03:00" value="03:00" />
                    <Picker.Item label="04:00" value="04:00" />
                    <Picker.Item label="05:00" value="05:00" />
                    <Picker.Item label="06:00" value="06:00" />
                    <Picker.Item label="07:00" value="07:00" />
                    <Picker.Item label="08:00" value="08:00" />
                    <Picker.Item label="09:00" value="09:00" />
                    <Picker.Item label="10:00" value="10:00" />
                    <Picker.Item label="11:00" value="11:00" />
                    <Picker.Item label="12:00" value="12:00" />
                    <Picker.Item label="13:00" value="13:00" />
                    <Picker.Item label="14:00" value="14:00" />
                    <Picker.Item label="15:00" value="15:00" />
                    <Picker.Item label="16:00" value="16:00" />
                    <Picker.Item label="17:00" value="17:00" />
                    <Picker.Item label="18:00" value="18:00" />
                    <Picker.Item label="19:00" value="19:00" />
                    <Picker.Item label="20:00" value="20:00" />
                    <Picker.Item label="21:00" value="21:00" />
                    <Picker.Item label="22:00" value="22:00" />
                    <Picker.Item label="23:00" value="23:00" /> 
                  </Picker>                               
                </View>               
              </View>             
           
              <View style={{ flexDirection: 'row' }}>             
                <View style={styles.picker}>             
                  <Picker selectedValue={this.props.horarioFim_2} onValueChange={horarioFim_2 => this.props.modificaHorarioFim_2(horarioFim_2)}>
                    <Picker.Item label="" value=""/>
                    <Picker.Item label="00:00" value="00:00" />
                    <Picker.Item label="01:00" value="01:00" />
                    <Picker.Item label="02:00" value="02:00" />
                    <Picker.Item label="03:00" value="03:00" />
                    <Picker.Item label="04:00" value="04:00" />
                    <Picker.Item label="05:00" value="05:00" />
                    <Picker.Item label="06:00" value="06:00" />
                    <Picker.Item label="07:00" value="07:00" />
                    <Picker.Item label="08:00" value="08:00" />
                    <Picker.Item label="09:00" value="09:00" />
                    <Picker.Item label="10:00" value="10:00" />
                    <Picker.Item label="11:00" value="11:00" />
                    <Picker.Item label="12:00" value="12:00" />
                    <Picker.Item label="13:00" value="13:00" />
                    <Picker.Item label="14:00" value="14:00" />
                    <Picker.Item label="15:00" value="15:00" />
                    <Picker.Item label="16:00" value="16:00" />
                    <Picker.Item label="17:00" value="17:00" />
                    <Picker.Item label="18:00" value="18:00" />
                    <Picker.Item label="19:00" value="19:00" />
                    <Picker.Item label="20:00" value="20:00" />
                    <Picker.Item label="21:00" value="21:00" />
                    <Picker.Item label="22:00" value="22:00" />
                    <Picker.Item label="23:00" value="23:00" /> 
                  </Picker>                               
                </View>                
              </View>
            </View>
          </View>

          <View style={styles.containerContato}>            
            <TextInput
              style={styles.lblContato}
              placeholder="Contato - Telefone:"
              value={this.props.telefone}
              onChangeText={telefone => this.props.modificaTelefone(telefone)}  
            />
            <TextInput
              style={styles.lblContato}
              placeholder="Contato - E-Mail:"
              value={this.props.email}
              onChangeText={email => this.props.modificaEmail(email)}
            />
          </View>

          <View style={styles.containerBotoes}>
            <TouchableHighlight style={styles.botao} underlayColor={'#d0f6ff'} onPress={() => this._cadastraClassificado()}>
              <Text style={styles.textoBotao}>Publicar</Text>
            </TouchableHighlight>

            <TouchableHighlight style={styles.botao} underlayColor={'#d0f6ff'} onPress={() => false}>
              <Text style={styles.textoBotao}>Cancelar</Text>
            </TouchableHighlight>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => (
  { 
    categoria:  state.CadastrarClassificadosReducer.categoria,
    subcategoria: state.CadastrarClassificadosReducer.subcategoria,
    titulo: state.CadastrarClassificadosReducer.titulo,
    descricao: state.CadastrarClassificadosReducer.descricao,
    horarioInicio_1: state.CadastrarClassificadosReducer.horarioInicio_1,
    horarioFim_1: state.CadastrarClassificadosReducer.horarioFim_1,
    horarioInicio_2: state.CadastrarClassificadosReducer.horarioInicio_2,
    horarioFim_2: state.CadastrarClassificadosReducer.horarioFim_2,
    telefone: state.CadastrarClassificadosReducer.telefone,
    email: state.CadastrarClassificadosReducer.email
  }
)

export default connect(
  mapStateToProps,
  {        
    modificaCategoria,
    modificaSubcategoria,
    modificaTitulo,
    modificaDescricao,
    modificaHorarioInicio_1,
    modificaHorarioFim_1,
    modificaHorarioInicio_2,
    modificaHorarioFim_2,
    modificaTelefone,
    modificaEmail,
    cadastraClassificado
  })(FormCadastroClassificados);

const styles = StyleSheet.create({
container:
  {
    flex: 1,
    padding: 10
  },
  containerTexto:
  {
    flexDirection: 'row'
  },
  containerLabel:
  {
    flexDirection: 'column',
    marginBottom: 10
  },
  lblSituacao:
  {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'right',
    marginBottom: 5
  },
  lblObs:
  {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'right',
    marginTop: 5
  },
  containerTextoLabel:
  {
    flexDirection: 'column',
    marginBottom: 15
  },
  texto:
  {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#545454'
  },
  containerFormulario:
  {  
    marginTop: 30
  },
  containerDescricao:
  {
    marginTop: 15,
    marginBottom: 30
  },
  descricao:
  {
    borderWidth: 1,
    borderColor: 'gray',
    backgroundColor: '#ffffff'  
  },
  containerPicker:
  {  
    flexDirection: 'row',
    marginBottom: 15,
    justifyContent: 'center'
  },
  picker:
  {
    width: 150,
    marginRight: 15,
    marginLeft: 15,
    paddingLeft: 10,
    backgroundColor: '#ffffff'
  },
  containerContato:
  {  
    marginTop: 20
  },
  lblContato:
  {
    marginBottom: 15,
    fontSize: 20,
    backgroundColor: '#ffffff'
  },
  containerBotoes:
  {
    marginTop: 15
  },
  botao:
  {
    alignItems: 'center',
    backgroundColor: '#b1f1ff',
    padding: 10,
    marginBottom: 10,
    borderWidth: 1
  },
  textoBotao:
  {
    fontSize: 20,
    color: '#3f3315'
  },
  txtLabel:
  {
    fontSize: 20,
    marginBottom: 5   
  },
  textInput:
  {
    fontSize: 20,
    marginTop: 30,
    marginBottom: 5,
    backgroundColor: '#ffffff'
  }
});
