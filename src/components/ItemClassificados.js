/*
Classe do componente ItemClassificados.
*/

import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';

import { BASE_PROTOCOL } from '../services/UrlApi';

export default class ItemClassificados extends Component {    
  render() {   
    return (      
      <View style={styles.container}>        
        <Text style={styles.txtTitulo}>{this.props.item.id} - {this.props.item.titulo}</Text>

        <View style={styles.containerImagem}>
          <Image style={styles.imagem} source={{ uri: `${BASE_PROTOCOL}` + this.props.item.img_link1 }} />
        </View>

        <View style={styles.containerDescricao}>
          <Text style={styles.txtDescricaoClassificado}>{this.props.item.texto}</Text>
          <Text style={styles.txtContato}><FontAwesome>{Icons.phone}</FontAwesome>  {this.props.item.contato_tel}</Text>
          <Text style={styles.txtContato}><FontAwesome>{Icons.envelope}</FontAwesome>  {this.props.item.contato_email}</Text>
          <Text style={styles.txtContato}><FontAwesome>{Icons.clockO}</FontAwesome>  {this.props.item.contato_hora}</Text>         
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#49c1ff',
    margin: 10,
    padding: 10,
    alignItems: 'center'
  },
  txtTitulo:
  {
    fontSize: 20,
    fontWeight: 'bold',    
    textAlign: 'left',
    color: '#498fff',
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10
  },
  containerImagem: {        
    width: 300,
    height: 300,
    marginBottom: 15   
  },
  imagem:
  {
    width: 300,
    height: 300   
  },
  containerDescricao:
  {
    marginTop: 15,
  },  
  txtDescricaoClassificado:
  {
    fontSize: 18,
    textAlign: 'left',
    fontWeight: 'bold',
    color: '#498fff',    
    marginBottom: 20,
    marginLeft: 10,
    marginRight: 10
  },
  txtContato:
  {
    fontSize: 18,
    textAlign: 'left',     
    color: '#498fff',
    marginLeft: 10
  }
});
