import React, { Component } from 'react';
import { ScrollView, StatusBar, StyleSheet, TextInput, View } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';

export default class ListaPodcasts extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.iconeContainer}>          
          <TextInput style={styles.input} placeholder="Digite o que procura" placeholderTextColor='#000000' />
          <FontAwesome style={styles.icone}>{Icons.search}</FontAwesome>
        </View>        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:
  {
    padding: 10
  },
  iconeContainer:
  {    
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  icone:
  {
    padding: 10,
    fontSize:20,
    color: '#000000'
  },
  input:
  { 
    flex: 1,    
    fontSize:20,
    textAlign: 'center'    
  },
  texto:
  {
    textAlign: 'left',
    marginBottom: 10,
    fontSize: 16,
    color: '#000000'
  }  
});
