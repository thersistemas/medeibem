import React, { Component } from 'react';
import { ScrollView, StatusBar, StyleSheet, TextInput, View } from 'react-native';

import Pesquisar from './Pesquisar';
import ListaPodCast from '../services/ListaPodCast';

export default class ViewListaPodCast extends Component {
  render() {
    return (
      <View>
        <Pesquisar />        
        <ListaPodCast />
      </View>
    );
  }
}