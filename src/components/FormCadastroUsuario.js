import React, { Component } from 'react';
import { Button, Image, Picker, ScrollView, StatusBar, StyleSheet, Text, TextInput, TouchableHighlight, View } from 'react-native';
import { connect } from 'react-redux';
import axios from 'axios';
import _ from 'lodash';

import { BASE_PROTOCOL, BASE_URL, BASE_API } from '../services/UrlApi';
import { 
  modificaNome,
  modificaSobrenome,
  modificaEmail,
  modificaRadarEstado,
  modificaRadarCidade,
  modificaSenha,
  cadastraUsuario
} from '../actions/CadastrarUsuarioActions';

class FormCadastroUsuario extends Component {

  constructor(props) {

    super(props);

    this.state = { 
      listaEstados: [], 
      listaCidades: []
    };
    
  }

  _cadastraUsuario() {

    const { nome, sobrenome, email, radarEstado, radarCidade, senha } = this.props;

    this.props.cadastraUsuario({ nome, sobrenome, email, radarEstado, radarCidade, senha });
  }

  /*_carregarEstados() {
    this.props.listarEstados();
  }*/

  componentWillMount() {

    axios.get(`${BASE_PROTOCOL}://${BASE_URL}/${BASE_API}?a=le`)
      .then(response => { this.setState({ listaEstados: response.data }); })
      .catch(() => { console.log('Erro ao recuperar os dodos!') });

  }

  onValueChangeCidades(idEstado) {

    this.props.modificaRadarEstado(idEstado);

    if (idEstado == '') {
      return this.setState({ listaCidades: [] });
    } else {
      axios.get(`${BASE_PROTOCOL}://${BASE_URL}/${BASE_API}?a=lc&uf=${idEstado}`)
      .then(response => { this.setState({ listaCidades: _.values(response.data) }); })
      .catch(() => { console.log('Erro ao recuperar os dodos!') });
    }    

  }
  
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <StatusBar backgroundColor='#5258aa' />

          <View style={styles.logo}>
            <Image source={require('../imgs/logo.png')} />
            <Text style={{ fontSize: 30, color: '#5258aa' }}>Cadastro</Text>
          </View>       

          <View style={styles.containerCampos}>
            <TextInput 
              style={{ fontSize: 20 }}
              placeholder="Nome"
              value={this.props.nome}
              onChangeText={nome => this.props.modificaNome(nome)}
            />
            <TextInput
              style={{ fontSize: 20 }}
              placeholder="Sobrenome"              
              value={this.props.sobrenome}
              onChangeText={sobrenome => this.props.modificaSobrenome(sobrenome)}
            />
            <TextInput
              style={{ fontSize: 20 }}
              placeholder="E-mail"
              value={this.props.email}
              onChangeText={email => this.props.modificaEmail(email)}
            />

            <Text style={{ fontSize: 20, marginTop: 20 }}>Radar-Estado</Text>
            <Picker selectedValue={this.props.radarEstado} onValueChange={this.onValueChangeCidades.bind(this)}>
              <Picker.Item label="" value="" />    
              {this.state.listaEstados.map((i, index) => (
                <Picker.Item key={index} label={i.estado} value={i.sigla} />  
              ))}         
            </Picker>
                        
            <Text style={{ fontSize: 20, marginTop: 10 }}>Radar-Cidade</Text>
            <Picker selectedValue={this.props.radarCidade} onValueChange={radarCidade => this.props.modificaRadarCidade(radarCidade)}>
              <Picker.Item label="" value="" />
              {this.state.listaCidades.map((i, index) => (
                <Picker.Item key={index} label={i.cidade_nome} value={i.idcidade} /> 
              ))}                       
            </Picker>

            <TextInput
              style={{ fontSize: 20 }}
              placeholder="Senha"
              value = {this.props.senha}
              onChangeText={senha => this.props.modificaSenha(senha)}
            />
            <TextInput
              style={{ fontSize: 20 }}
              placeholder="Confirmação de Senha"
              value={this.props.senha}
              onChangeText={senha => this.props.modificaSenha(senha)}
             />
          </View>

          <View style={styles.containerBotoes}>
            <TouchableHighlight style={styles.botao} underlayColor={'#FFDD8A'} onPress={() => this._cadastraUsuario()}>
              <Text style={styles.textoBotao}>Cadastrar</Text>
            </TouchableHighlight>
            
            <TouchableHighlight style={styles.botao} underlayColor={'#FFDD8A'} onPress={() => false}>
              <Text style={styles.textoBotao}>Limpar</Text>
            </TouchableHighlight>
          </View>

        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({  
  nome: state.CadastrarUsuarioReducer.nome,
  sobrenome: state.CadastrarUsuarioReducer.sobrenome,
  email: state.CadastrarUsuarioReducer.email,
  radarEstado: state.CadastrarUsuarioReducer.radarEstado,
  radarCidade: state.CadastrarUsuarioReducer.radarCidade,
  senha: state.CadastrarUsuarioReducer.senha,
  erroCadastroUsuario: state.CadastrarUsuarioReducer.erroCadastroUsuario
})

export default connect(
  mapStateToProps, 
  {
    modificaNome,
    modificaSobrenome,
    modificaEmail,
    modificaRadarEstado,
    modificaRadarCidade,
    modificaSenha,
    cadastraUsuario
  })(FormCadastroUsuario);

const styles = StyleSheet.create({
  container:
  {
    flex: 1,
    padding: 10
  },
  logo:
  {
    marginTop: 30,    
    alignItems: 'center',
    flex: 1    
  },
  containerCampos:
  {
    flex: 2,
    marginTop: 50
  },
  containerBotoes:
  {
    flex: 2,
    marginTop: 50
  },
  botao:
  {
    alignItems: 'center',
    backgroundColor: '#ffd56d',
    padding: 10,
    marginBottom: 10,
  },
  textoBotao:
  {
    fontSize: 20,
    color: '#3f3315'
  }
});