import React, { Component } from 'react';
import { ScrollView, StatusBar, StyleSheet, TextInput, View, Text } from 'react-native';
import axios from 'axios';
import { openDatabase } from "react-native-sqlite-storage";

import { BASE_PROTOCOL, BASE_URL, BASE_API } from '../services/UrlApi';
import ItemDetalhePromocao from '../components/ItemDetalhePromocao'

export default class CarregaDetalhePromocao extends Component {

  constructor(props) {
    super(props);
    this.state = { detalhePromocao: [] };
  }

  componentWillMount() {   
    
    let db = openDatabase({name: 'medeibem.db', createFromLocation: "~me_dei_bem.db"});  

    var appKey = '';
  
    db.transaction((tx) => {

      tx.executeSql('SELECT token FROM USER LIMIT 1', [], (tx, results) => {

        appKey = results.rows.item(0).token;

        axios.get(`${BASE_PROTOCOL}://${BASE_URL}/${BASE_API}?a=lprm&k=${appKey}&d={%22range_ini%22:%220%22,%22range_fim%22:%2210%22,%22busca%22:%22Hamburgers%20Jato%20Expresso!%22}`)
          .then(response => { this.setState({ detalhePromocao: response.data }); })
          .catch(() => { console.log('Erro ao recuperar os dodos!') });
      
      });
      
    });
    
  }
  render() {   
    return (
      <View>
        <ScrollView>      
          { this.state.detalhePromocao.map(item => (<ItemDetalhePromocao key={ item.id } item={ item } />)) }
        </ScrollView>
      </View>      
    );
  }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#49c1ff',
    margin: 10,
    padding: 10,
    alignItems: 'center'
  },
  containerImagem: {       
    width: 250,
    height: 250,    
  },
  imagem:
  {
    width: 250,
    height: 250
  },
  detalhesItem:
  {
    marginLeft: 20,
    flex: 1
  },
  txtTitulo:
  {
    fontSize: 20,
    fontWeight: 'bold',    
    textAlign: 'center',
    color: '#494949',
    marginBottom: 5
  },
  txtValor:
  {
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'bold'   
  },
  txtDetalhes:
  {
    fontSize: 16
  }
});