//Carrega uma lista de promoções.

import React, { Component } from 'react';
import { ScrollView, StatusBar, StyleSheet, TextInput, View, Text } from 'react-native';
import axios from 'axios';
import { openDatabase } from "react-native-sqlite-storage";

import { BASE_PROTOCOL, BASE_URL, BASE_API } from '../services/UrlApi';
import ItemPromocao from '../components/ItemPromocao';

export default class ListaPromocao extends Component {

  constructor(props) {
    super(props);
    this.state = { listaPromocao: [] };
  }

  componentDidMount() {

    let db = openDatabase({name: 'medeibem.db', createFromLocation: "~me_dei_bem.db"});  

    var appKey = '';
  
    db.transaction((tx) => {

      tx.executeSql('SELECT token FROM USER LIMIT 1', [], (tx, results) => {

        appKey = results.rows.item(0).token;

        axios.get(`${BASE_PROTOCOL}://${BASE_URL}/${BASE_API}?a=lclf&k=${appKey}&d={%22range_ini%22:%220%22,%22range_fim%22:%22100%22,%22busca%22:%22%22}`)
          .then(response => { this.setState({ listaPromocao: response.data }); })
          .catch(() => { console.log('Erro ao recuperar os dodos!') });
      
      });
      
    });
    
  }  

    
  render() {
    return (
      <View style={{ height: 530 }}>
        <ScrollView>      
          { this.state.listaPromocao.map(item => (<ItemPromocao key={ item.id } item={ item } />)) }
        </ScrollView>
      </View>
    );
  }
} 

const styles = StyleSheet.create({
  container:
  {
    padding: 10,    
    flexDirection: 'column',
    backgroundColor: 'blue'
  },
  iconeContainer:
  {    
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',   
    backgroundColor: 'red',
    flex: 1
  },
  icone:
  {
    padding: 10,
    fontSize:20,
    color: '#000000'
  },
  input:
  { 
    flex: 1,    
    fontSize:20,
    textAlign: 'center'    
  },
  texto:
  {
    textAlign: 'left',
    marginBottom: 10,
    fontSize: 16,
    color: '#000000'
  }  
});