import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { Actions } from 'react-native-router-flux';

import Rotas from './src/Rotas';
import reducers from './src/reducers';
import { openDatabase } from 'react-native-sqlite-storage';

class App extends Component {

  componentWillMount() {
    
    let db = openDatabase({name: 'medeibem.db', createFromLocation: "~me_dei_bem.db"});

    db.transaction((tx) => {
      tx.executeSql('SELECT ind_logado FROM USER LIMIT 1', [], (tx, results) => { 

        let dados = results.rows.length  
        console.log(results.rows); 

        for (let i = 0; i < dados; i++) { 

          console.log(results.rows.item(i));       
          let row = results.rows.item(i);
          if (row.ind_logado === 1) {                                             
            Actions.funcoes();            
          } 

        }

      });
      
    });  
    
  }

  render() {
    return ( 
      <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>     
        <Rotas />
      </Provider>      
    );
  }
}

export default App;
